/////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////
//////                                                             //////
//////               Read Me                                       //////
//////                                                             //////
//////       Auto configuration Tool                               //////
//////       version 2.0                                           //////
//////       Written by Evan W. Lawrence                           //////
//////                                                             //////
/////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////


///////////
Q. What does it do?

A. This software is designed to automate a good portion of the SNHU build process when configuring employee laptops for deployment. 
The intention is to greatly reduce the time required and guarantee accuracy



Q. Okay, but what does IT DO?
A. Specifically, this software is designed to:

-Name the computer
-Join the computer to the SNHU.edu domain
-Add user to Administrators group
-Add user's department to power users group.
-Install printer based on user's department ***
-Configure Imagenow
-Pins Firefox and Office suite to taskbar
-Configures Internet Explorer
-Configures Wi-Fi profile
-Sets desktop background (requires restart to take effect)
-Adds default network places / shared folders
-Opens networkregistration.snhu.edu/registration for prompted registration to the network
-Opens AgentInstall64.msi, RemoteAccess_VPN_E80.41_Windows.msi, and PGPDesktop64_en-us.msi for prompted installation
-Opens several programs for prompted configuration, and alerts the user with a reminder (ie. WMP, Firefox, Outlook)


/////////////
Instructions/
/////////////
1. Sign in to your ADM account and run "Step 1 - PC Name and Domain.exe"
2. If successful, restart computer
3. Sign in to your ADM account again and run "Step 2 - User Permissions.exe"
4. Sign out and sign in to employee's profile
5. Follow on-screen prompts
/////////////


Please email any questions, comments or suggestions to e.lawrence3@snhu.edu

Thank you and enjoy!




***The developer requires further information to improve upon this feature


