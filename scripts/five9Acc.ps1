param([string]$username, [string]$password, [string]$stationID, [string]$autoLogin)

$ie = New-Object -com InternetExplorer.Application 
$ie.visible="false"
$ie.navigate("https://us1.five9.com/login/") 

while($ie.ReadyState -ne 4) {start-sleep -m 100} 
$ie.document.getElementById("input_username").value = "$username"
$ie.document.getElementById("input_password").value = "$password" 
$ie.document.getElementById("input_login_submit").click()
start-sleep -m 1000
while($ie.ReadyState -ne 4) {start-sleep -m 100} 
if($ie.document.body.contains($ie.document.getElementById("msf_sec_question")))
{
$ie.document.getElementById("msf_sec_question").selectedIndex = "4"
$ie.document.getElementById("msf_sec_question").value = "4"
$ie.document.getElementById("msf_sec_question").onchange
$ie.document.getElementById("msf_sec_answer").readonly = "false"
$ie.document.getElementById("msf_sec_answer").removeAttribute("readonly")
$ie.document.getElementById("msf_sec_answer").value = "Manchester"
$ie.document.getElementById('msf_sec_ans_entered').value  = "Manchester"
$ie.document.getElementById("msf_update").disabled = "false"
$ie.document.getElementById("msf_update").removeAttribute("disabled")
$ie.document.getElementById("msf_update").click()
start-sleep -m 1000
while($ie.ReadyState -ne 4) {start-sleep -m 100} 

}
$ie.navigate("https://us1.five9.com/settings/#tab3") 
start-sleep -m 1000
while($ie.ReadyState -ne 4) {start-sleep -m 100} 

$ie.document.getElementById("msf_agent_station").removeAttribute("disabled")
$ie.document.getElementById("msf_agent_station").value = "$stationID"
$ie.document.getElementById("msf_auto_login").checked = "$autoLogin"
$validation = $?
$validation

$ie.document.getElementById("alf_submit").click()
start-sleep -m 1000
while($ie.ReadyState -ne 4) {start-sleep -m 100} 
$ie.document.getElementById("page_logout").click()
start-sleep -m 1000
while($ie.ReadyState -ne 4) {start-sleep -m 100} 
$wshell = New-Object -ComObject Wscript.Shell
$ie.Quit()
exit $validation
