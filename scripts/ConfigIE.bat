@echo off
  setlocal
  set qry="HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Internet Settings\Zones\1"
  reg add %qry% /v 1609 /t REG_DWORD /d 0 /f
  reg add %qry% /v 2500 /t REG_DWORD /d 3 /f
  set qry="HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Internet Settings\Zones\2"
  reg add %qry% /v 1609 /t REG_DWORD /d 0 /f
  reg add %qry% /v 2500 /t REG_DWORD /d 3 /f
  set qry="HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Internet Settings\Zones\3"
  reg add %qry% /v 1609 /t REG_DWORD /d 0 /f
  reg add %qry% /v 2500 /t REG_DWORD /d 3 /f
  REG ADD "HKEY_CURRENT_USER\SOFTWARE\MICROSOFT\INTERNET EXPLORER\MAIN" /V "START PAGE" /D "http://my.snhu.edu/" /F 
  REG ADD "HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Internet Settings\ZoneMap\Domains\snhu.edu" /F
  set qry="HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Internet Settings\ZoneMap\Domains\snhu.edu"
  reg add %qry% /v * /t REG_DWORD /d 2 /f
  REG ADD "HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Internet Settings\ZoneMap\Domains\five9.com" /F
  set qry="HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Internet Settings\ZoneMap\Domains\five9.com"
  reg add %qry% /v * /t REG_DWORD /d 2 /f
  reg add "HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Internet Settings\ZoneMap\Domains\snhu.edu" /v "" /f
  set qry="HKEY_CURRENT_USER\Software\Microsoft\Internet Explorer\New Windows" 
  reg add %qry% /v PopupMgr /t REG_DWORD /d 0 /f

exit