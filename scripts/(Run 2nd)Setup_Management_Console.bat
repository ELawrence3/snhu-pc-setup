@echo off
set /p UNAME="Enter User's SNHU email (without '.snhu') : " %=%

echo Enter '1' for Advising & echo.Enter '2' for Admissions && echo Enter '3' for Academics && echo Enter '4' for MSR - DATA && echo Enter '5' for MSR - WEB && echo Enter '6' for PC Services
@echo off
set /p DEPT="Select User's Department: "%=%
@echo off
net localgroup administrators snhu\%UNAME% /add
powershell -command "& { ([adsi]'WinNT://./Administrators,group').Add('WinNT://snhu/Workstation Admins Users Group,group'); }"

IF %DEPT%==1 (set VAR=Advising Team - COCE and UC)
IF %DEPT%==2 (set VAR=Admissions Users Group)
IF %DEPT%==3 (set VAR=Academic Affairs Users Group)
IF %DEPT%==4 (set VAR=MSR Data Services Admin Group)
IF %DEPT%==5 (set VAR=MSR Web Services Admin Group)
IF %DEPT%==6 (set VAR=PC Services Group)
powershell -command "& { ([adsi]'WinNT://./Power Users,group').Add('WinNT://snhu/%VAR%,group'); }"
net user delete /del