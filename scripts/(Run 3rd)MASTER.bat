echo Enter '1' for Advising & echo.Enter '2' for Admissions && echo Enter '3' for Academics && echo Enter '4' for MSR && echo Enter '5' for Administration && echo Enter '6' for PC Services
@echo off
set /p DEPT="Select User's Department: "%=%

IF %DEPT%==1 (set VAR=Advising_Printers)
IF %DEPT%==2 (set VAR=Admissions_Printers)
IF %DEPT%==3 (set VAR=Academics_Printers)
IF %DEPT%==4 (set VAR=Academics_Printers)
IF %DEPT%==6 (set VAR=Administration_Printers)
IF %DEPT%==6 (set VAR=ITS_Printers)

pushd "%~dp0addPrinters"
start %VAR%.bat
popd

pushd "%~dp0Imagenow"
start ConfigImageNow.bat
popd

pushd "%~dp0"
start Agnt_VPN_PGP_Network.bat
start AddShortcuts.bat
start ConfigIE.bat
start SetBackground.bat
start networkplaces.vbs
popd

cd "%ProgramFiles%\Windows Media Player"
start wmplayer.exe

call "C:\Program Files\Microsoft Office\Office15\lync.exe"

cd "C:\Program Files\Microsoft Office\Office15"
start OUTLOOK.exe

cd "C:\Program Files (x86)\Mozilla Firefox\"
Start firefox.exe
popd

call %~dp0reminder.txt
exit