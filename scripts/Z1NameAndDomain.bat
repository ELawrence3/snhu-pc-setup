@echo off
REG ADD HKLM\SYSTEM\CurrentControlSet\Control\ComputerName\ComputerName /v ComputerName /t REG_SZ /d %PCNAME% /f
REG ADD HKLM\SYSTEM\CurrentControlSet\Control\ComputerName\ComputerName /v ComputerName /t REG_SZ /d %PCNAME% /f
REG ADD HKLM\SYSTEM\CurrentControlSet\Control\ComputerName\ActiveComputerName\ /v ComputerName /t REG_SZ /d %PCNAME% /f
REG ADD HKLM\SYSTEM\CurrentControlSet\Services\Tcpip\Parameters\ /v Hostname /t REG_SZ /d %PCNAME% /f
REG ADD HKLM\SYSTEM\CurrentControlSet\Services\Tcpip\Parameters\ /v "NV Hostname" /t REG_SZ /d %PCNAME% /f

NETDOM /Domain:snhu.edu /user:%ADM% /password:%PASS% MEMBER
%PCNAME% /JOINDOMAIN


@echo off
if %USERDOMAIN%==SNHU (cscript scripts\Message.vbs "Successfully joined %USERDOMAIN% Domain! Please restart the computer before proceeding to Step 2 - User Permissions.exe") else (cscript scripts\Message.vbs "ERROR: Please Verify Credentials and Network Connection, then try again.")
exit
