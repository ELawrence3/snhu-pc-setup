@Echo Off 
REM Change \\staffprintsrv\PRINTER by your printer's UNC

REM Add printer 
rundll32 printui.dll,PrintUIEntry /in /n"\\staffprintsrv\Millyard 3rd Floor Advising Annex Toshiba"

REM Add printer 
rundll32 printui.dll,PrintUIEntry /in /n"\\staffprintsrv\Millyard 3rd Floor Student Advisor Meeting Area Toshiba"

REM Add printer 
rundll32 printui.dll,PrintUIEntry /in /n"\\staffprintsrv\Millyard 3rd Floor Advising Lexmark"

REM Add printer
rundll32 printui.dll,PrintUIEntry /in /n"\\staffprintsrv\Millyard 3rd Floor Instructional Design Toshiba"

REM Set printer as default 
rundll32 printui.dll,PrintUIEntry /y /n"\\staffprintsrv\Millyard 3rd Floor Advising Lexmark"

exit