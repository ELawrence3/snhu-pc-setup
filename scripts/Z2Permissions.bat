@echo off
net localgroup administrators snhu\%UNAME% /add
powershell -command "& { ([adsi]'WinNT://./Administrators,group').Add('WinNT://snhu/Workstation Admins Users Group,group'); }"
powershell -command "& { ([adsi]'WinNT://./Power Users,group').Add('WinNT://snhu/%DEPT%,group'); }"
net user delete /del
echo off

IF "%DEPT%"=="Advising Team - COCE and UC" (set VAR=Advising_Printers)
IF "%DEPT%"=="Admissions Users Group" (set VAR=Admissions_Printers)
IF "%DEPT%"=="Academic Affairs Users Group" (set VAR=Academics_Printers)
IF "%DEPT%"=="MSR Data Services Admin Group" (set VAR=Academics_Printers)
IF "%DEPT%"=="MSR Web Services Admin Group" (set VAR=Academics_Printers)
IF "%DEPT%"=="Administration" (set VAR=Administration_Printers)
IF "%DEPT%"=="PC Services Group" (set VAR=ITS_Printers)

set step3="Z3MASTERConfig.bat"

del %step3%
echo pushd "%~dp0addPrinters" >> %step3%
echo start %VAR%.bat >> %step3%
echo popd >> %step3%

echo pushd "%~dp0Imagenow" >> %step3%
echo start ConfigImageNow.bat >> %step3%
echo popd >> %step3%

echo cscript PinItem.vbs /taskbar /item:"%ProgramData%\Microsoft\Windows\Start Menu\Programs\Mozilla Firefox.lnk" >> %step3%
echo cscript PinItem.vbs /taskbar /item:"%ProgramData%\Microsoft\Windows\Start Menu\Programs\Microsoft Office 2013\Outlook 2013.lnk" >> %step3%
echo cscript PinItem.vbs /taskbar /item:"%ProgramData%\Microsoft\Windows\Start Menu\Programs\Microsoft Office 2013\Word 2013.lnk" >> %step3%
echo cscript PinItem.vbs /taskbar /item:"C:\Program Files\Microsoft Office\Office15\lync.exe" >> %step3%
echo cscript PinItem.vbs /taskbar /item:"%ProgramData%\Microsoft\Windows\Start Menu\Programs\Microsoft Office 2013\Excel 2013.lnk" >> %step3%

echo pushd "%~dp0" >> %step3%
echo start Agnt_VPN_PGP_Network.bat >> %step3%
echo start ConfigIE.bat >> %step3%
echo reg add "HKCU\Control Panel\Desktop" /v Wallpaper /f /t REG_SZ /d "C:\Windows\Web\Wallpaper\SNHULogo medium corner.bmp" >> %step3%
echo start networkplaces.vbs >> %step3%
echo popd >> %step3%

echo cd "%ProgramFiles%\Windows Media Player" >> %step3%
echo start wmplayer.exe >> %step3%

echo call "C:\Program Files\Microsoft Office\Office15\lync.exe" >> %step3%
echo call "%ProgramData%\Microsoft\Windows\Start Menu\Programs\Mozilla Firefox.lnk" %step3%

echo cd "C:\Program Files\Microsoft Office\Office15" >> %step3%
echo start OUTLOOK.exe >> %step3%
echo popd >> %step3%

echo del "C:\Users\%UNAME%\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup\step3.lnk" >> %step3%

echo pushd "%~dp0" >> %step3%
echo cscript Message.vbs "DONT FORGET TO: Add Computer and User's documents to desktop and sort desktop icons by name (x2)" >> %step3%
echo popd >> %step3%
echo del %0 >> %step3%


if not exist "C:\Users\%UNAME%\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup" mkdir "C:\Users\%UNAME%\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup"

set SCRIPT="%TEMP%\%RANDOM%-%RANDOM%-%RANDOM%-%RANDOM%.vbs"

echo Set oWS = WScript.CreateObject("WScript.Shell") >> %SCRIPT%
echo sLinkFile = "C:\Users\%UNAME%\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup\step3.lnk" >> %SCRIPT%
echo Set oLink = oWS.CreateShortcut(sLinkFile) >> %SCRIPT%
echo oLink.TargetPath = "%~dp0Z3MASTERConfig.bat" >> %SCRIPT%
echo oLink.Save >> %SCRIPT%

cscript /nologo %SCRIPT%
del %SCRIPT%

cscript scripts\Message.vbs "User added successfully. Please sign in as user to continue to Step 3 - Software Configuration."
exit